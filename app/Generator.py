from app.modules.file_helper import save_file
from app.modules.random_string_generator import generate, get_allowed_characters


class Generator:
    def __init__(self, regex):
        self.allowed_characters = get_allowed_characters(regex)
        self.strings = []

    def generate(self, count, length):
        print('Generating, please wait...')

        for i in range(0, count):
            string = generate(self.allowed_characters, length)
            self.strings.append(string)

    def save_to_file(self, filename):
        save_file(filename, self.strings)
        print('File "%s" has been successfully created.' % filename)
