import time
import resource


class Benchmark:
    def __init__(self):
        self.start_time = 0
        self.end_time = 0
        self.start_memory = 0
        self.end_memory = 0

    def start(self):
        print('Processing, please wait...')
        self.start_time = time.time()
        self.start_memory = self.get_memory_usage()

    def stop(self):
        self.end_time = time.time()
        self.end_memory = self.get_memory_usage()
        print('Done.')

    @staticmethod
    def get_memory_usage():
        return resource.getrusage(resource.RUSAGE_SELF).ru_maxrss

    def print_result(self):
        total_time = self.end_time - self.start_time
        total_memory = (self.end_memory - self.start_memory) / 1024
        print('*** BENCHMARK RESULT ***')
        print('Total time: %fs' % total_time)
        print('Memory usage: %dKB' % total_memory)
