import random


def sort(array):
    qsort(array, 0, len(array) - 1)


def qsort(array, begin, end):
    if begin >= end: return

    i, j = begin, end
    pivot = array[random.randint(begin, end)]

    while i <= j:
        while array[i] < pivot:
            i += 1
        while array[j] > pivot:
            j -= 1
        if i <= j:
            array[i], array[j] = array[j], array[i]
            i, j = i + 1, j - 1
    qsort(array, begin, j)
    qsort(array, i, end)
