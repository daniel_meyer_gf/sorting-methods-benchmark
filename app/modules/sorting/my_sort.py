

def sort(array):
    bubble(array)


def bubble(array):
    length = len(array) - 1
    is_sorted = False

    while not is_sorted:
        is_sorted = True
        for i in range(length):
            if array[i] > array[i + 1]:
                is_sorted = False
                array[i], array[i + 1] = array[i + 1], array[i]
