from importlib import import_module

sorting_methods = ('my_sort', 'qsort', 'python')


def get_method(method):
    if method in sorting_methods:
        return import_module('app.modules.sorting.' + method)
    else:
        raise Exception('Unknown sorting method: %s' % method)
