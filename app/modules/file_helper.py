

def load_file(filename):
    file = open(filename, 'r')
    content = file.read()
    file.close()
    return content.split('\n')


def save_file(filename, content):
    file = open(filename, 'w')
    file.write('\n'.join(content))
    file.close()
