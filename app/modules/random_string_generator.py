import string
import random
import re


def generate(allowed_characters, length):
    return ''.join(random.choices(allowed_characters, k=length))


def get_allowed_characters(pattern):
    fixed_pattern = pattern + '+'
    all_chars = string.ascii_lowercase + string.ascii_uppercase + string.digits
    try:
        result = re.search(fixed_pattern, all_chars)
    except re.error:
        raise Exception('Pattern not allowed.')
    if result is None:
        raise Exception('Pattern not allowed')
    return result[0]
