import sys
from app.modules.file_helper import load_file, save_file
from app.modules.sorting_methods import get_method, sorting_methods
from app.Benchmark import Benchmark


def main(argv):
    if check_params(argv) is False:
        return
    _, sorting_method, input_filename, output_filename = argv
    content = load_file(input_filename)
    benchmark = Benchmark()
    benchmark.start()
    get_method(sorting_method).sort(content)
    benchmark.stop()
    benchmark.print_result()
    save_file(output_filename, content)


def check_params(argv):
    if len(argv) < 4:
        print('Benchmark requires 3 parameters: <sorting_method> <input_filename> <output_filename>')
        print('For example:')
        print('python benchmark.py qsort input.txt output.txt')
        return False
    if argv[1] not in sorting_methods:
        print('Use sorting methods from list: %s' % ', '.join(sorting_methods))
        return False
    return True


if __name__ == '__main__':
    main(sys.argv)
