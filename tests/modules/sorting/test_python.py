import unittest
from app.modules.sorting.python import sort


class TestPythonSort(unittest.TestCase):
    def test_sort(self):
        array = [
            'WSz87iHkTc4xhVTDwP8Q00bV4nX6jCXZ',
            'm0eikW1cmraNYYxJW275XnHmW3ge1y5K',
            'HQJPdPd7QhsnFnisKwh0FsHH06BoNVcj',
            'N49Brtj9tRAbCM8jq80k41o5G8tXtMh0',
            '7ACiLDEkq9jtOcucUuvfLfPvBk3p1ZDq',
            'uO7rYNj1IOboJqfTgi4d2z7rAmV1ni5j',
            'gHrB0WDRcRePZwegWdYofrs75gTT8Pgp',
            '3Qoiw2DusUz9r3VvbV19GVepNIO6bOWB',
            'I1Dv80kH3SI1QopD6Yu1yek8aGNWuDqz',
            'iCPlvYPX8bMFwaI576sd5CnBjJmU7rgY'
        ]
        sort(array)
        self.assertEqual(array, [
            '3Qoiw2DusUz9r3VvbV19GVepNIO6bOWB',
            '7ACiLDEkq9jtOcucUuvfLfPvBk3p1ZDq',
            'HQJPdPd7QhsnFnisKwh0FsHH06BoNVcj',
            'I1Dv80kH3SI1QopD6Yu1yek8aGNWuDqz',
            'N49Brtj9tRAbCM8jq80k41o5G8tXtMh0',
            'WSz87iHkTc4xhVTDwP8Q00bV4nX6jCXZ',
            'gHrB0WDRcRePZwegWdYofrs75gTT8Pgp',
            'iCPlvYPX8bMFwaI576sd5CnBjJmU7rgY',
            'm0eikW1cmraNYYxJW275XnHmW3ge1y5K',
            'uO7rYNj1IOboJqfTgi4d2z7rAmV1ni5j'
        ])


if __name__ == '__main__':
    unittest.main()
