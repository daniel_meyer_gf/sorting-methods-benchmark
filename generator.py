#!/usr/bin/python

import sys
from app.Generator import Generator


def main(argv):
    if check_params(argv) is False:
        return

    _, regex, string_count, string_length, output_filename = argv

    try:
        generator = Generator(regex)
        generator.generate(int(string_count), int(string_length))
        generator.save_to_file(output_filename)
    except Exception as e:
        print(e)


def check_params(argv):
    if len(argv) < 5:
        print('Generator requires 4 parameters: <regex> <string_count> <string_length> <output_filename>')
        print('For example:')
        print('python generator.py [0-9a-zA-Z] 10000 32 data.txt')
        return False
    return True


if __name__ == '__main__':
    main(sys.argv)
